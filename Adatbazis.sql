-- A feladatok megold�sa sor�n a feladatlap k�n�lta inform�ci�k alapj�n felt�telezem, hogy 

-- Feladat 1: Sz�ks�g�nk van minden userre egyszer a list�nkban mely mindk�t t�bl�ban el�fordul, Login-ra,  a n�vre, �s a val�di utols� login d�tumra

select * from User_A
intersect
select * from User_B

-- Feladat 2: Sz�ks�g�nk van minden userre mely valamelyik t�bl�b�l hi�nyzik, Login-ra,  a n�vre, �s a val�di utols� login d�tumra

select 
case when Login is not null then Login else Login_ID end as Login,
case when Name is not null then Name else Login_ID end as Name,
case when Lastlogin is not null then Lastlogin else LoginDate end as LastLogin
from 
(select * 
from User_A Asa full outer join User_B B on Asa.Login = B.Login_ID
where Asa.Login IS NULL OR B.Login_ID IS NULL);
