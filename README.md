# IT development szakmai teszt - Gyakornok #

### Feladat ###

* Szakmai feladatsor, aminek a megold�s�hoz 24 �ra �ll rendelkez�sedre.
* A megold�sokhoz b�rmilyen seg�deszk�zt felhaszn�lhatsz. 
* A v�laszokat elektronikus form�ban v�rjuk vissza.

### I. Adatb�zis ###

Adott 2 hasonl� t�bla - nem minden oszlop van felsorolva

User_A
Login (PRIMARY KEY CLUSTERED) [nvarchar](32) NOT NULL
Name [nvarchar](128) NOT NULL
Lastlogin [datetime2](7) NULL

User_B
Login_ID (PRIMARY KEY CLUSTERED) [nvarchar](64) NOT NULL
Full_name [nvarchar](96) NOT NULL
LoginDate [datetime] NULL

T�telezz�k fel, hogy a k�t t�bla azonos COLLATION-t haszn�l.

* Feladat 1: Sz�ks�g�nk van minden userre egyszer a list�nkban mely mindk�t t�bl�ban el�fordul, Login-ra,  a n�vre, �s a val�di utols� login d�tumra
* Feladat 2: Sz�ks�g�nk van minden userre mely valamelyik t�bl�b�l hi�nyzik, Login-ra,  a n�vre, �s a val�di utols� login d�tumra

### II. Programoz�s ###

K�sz�tsen egy konzolos c# alkalmaz�st, mely a k�vetkez�ket tudja:

Feladat 1:  Fel kell t�lteni egy 7 elemsz�m� list�t a k�vetkez� szab�lyok szerint:

* Kell egy d�nt�si pont, hogy a k�vetkez� elem sz�m, vagy sz�veg lesz.
* Ha sz�mot ad meg, abban az esetben 10 �s 9999 k�z�tti eg�sz sz�mot fogadjon el.
* Ha sz�veget ad meg, abban az esetben a bek�rt sz�veg hossza 5 �s 45 k�z�tt legyen.
* Extra: Amennyiben rossz inputot adott meg, figyelmeztesse a felhaszn�l�t, majd ism�telje meg a bek�r�st.

Feladat 2:  A 7 elem bek�r�s�nek befejez�se ut�n a k�vetkez� outputot kell megjelen�teni:

* A bek�rt sz�mok eset�n a p�ros sz�mokat ossza el 2-vel a p�ratlanokat szorozza kett�vel. Extra: jel�lje a pr�msz�mokat
* A bek�rt sz�vegekek eset�n f�zz�n hozz� annyi karakter a k�vetkez� sz�vegb�l amilyen hossz� a karakterl�nc � �Making an impact that matters �Deloitte�

Minta.

Sz�m (3)
13 � 26 !pr�msz�m
55 - 110
66 - 33

Sz�veg (2)
Deloitte - Making a
SQL - Mak